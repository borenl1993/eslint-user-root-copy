// Rule ID react/jsx-no-target-blank
var TargetBlank = <Link target="_blank" to="http://example.com/"></Link>

// Rule ID react/no-unescaped-entities
var UnescapedEntity = <MyComponent>{'Text'}}</MyComponent>

// Rule ID react/no-unsafe
class UnsafeComponentUsed extends React.Component {
  UNSAFE_componentWillUpdate() {}
}

// Rule ID react/no-danger
var Hello = <div dangerouslySetInnerHTML={{ __html: "Hello World" }}></div>;

// Rule ID react/no-danger-with-children
React.createElement("div", { dangerouslySetInnerHTML: { __html: "HTML" } }, "Children");

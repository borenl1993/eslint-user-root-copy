FROM node:11-alpine

RUN apk add ca-certificates openssl git bash

WORKDIR /home/node

COPY --chown=root:root analyzer /
COPY --chown=node:node eslintrc ./.eslintrc
COPY --chown=node:node babel.config.json .
COPY --chown=node:node package.json .
COPY --chown=node:node yarn.lock .

RUN yarn --frozen-lockfile && yarn cache clean

ENTRYPOINT []
CMD ["/analyzer", "run"]

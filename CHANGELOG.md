# ESLint analyzer changelog

## v2.0.4
- Use babel-parser to support Stage-0 ES syntax

## v2.0.3
- Add eslint-plugin-react in support of React projects

## v2.0.2
- Update common to v2.1.6

## v2.0.1
- Ignore `.eslintrc` files in the repo

## v2.0.0
- Switch to new report syntax with `version` field

## v1.0.1
- Fix missing `.eslintrc` file when `$HOME` is not set to `/home/node`

## v1.0.0
- Initial release
